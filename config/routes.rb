Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'apps#index'
  get '/test' => 'apps#test'
  get '/routing' => 'apps#route'
  get '/token' => 'apps#token'
  get '/pedro' => 'apps#pedro'
end
