class AppsController < ApplicationController

	def index
	
	end

	def test
		@app = App.new()
		respond_to do |format|
	    format.json {render json: @app.test}
	    format.html {render json: @app.test}
    end
	end

	def route
		@app = App.new()
		respond_to do |format|
	    format.json {render json: @app.routing}
	    format.html {render json: @app.routing}
    end
	end

	def token
		@app = App.new()
 		respond_to do |format|
	    format.json {render json: @app.rest_client}
	    format.html {render json: @app.rest_client}
    end
	end
end
