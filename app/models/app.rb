class App < ApplicationRecord
	
	require 'uri'
	require 'net/http'

	def getbase
		Base64.encode64('academiahack:YWNhZGVtaWFoYWNrOjQzNTFkNDBmMzAzMGFhZDIyMDdmZTU3MTUyMjk4MTFiNzFjN2Y2ZWU=')
	end

	def rest_client()
		url = URI("https://api.intraffic.com.ve/oauth2/token")
		http = Net::HTTP.new(url.host, url.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		request = Net::HTTP::Post.new(url)
		request["content-type"] = 'application/json'
		request["authorization"] = 'Basic YWNhZGVtaWFoYWNrOjQzNTFkNDBmMzAzMGFhZDIyMDdmZTU3MTUyMjk4MTFiNzFjN2Y2ZWU='
		request["cache-control"] = 'no-cache'
		request["postman-token"] = 'bf1ed8d4-be79-817b-4787-480daf8dc565'
		request.body = "{\n    \"grant_type\": \"client_credentials\",\n    \"dataType\": \"json\",\n    \"contentType\": \"application/json\",\n    \"async\": \"true\",\n    \"cache\": \"false\"\n}"
		response = http.request(request)
		return response.read_body

	end

	def test
		url = URI("https://api.intraffic.com.ve/routes/public/afb25833-e2b2-4a96-8a20-a4b268b82c73.geojson")
		http = Net::HTTP.new(url.host, url.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE

		request = Net::HTTP::Get.new(url)
		request["user-agent"] = 'develop'
		request["authorization"] = 'Bearer ea428bc36fbc4f46ed50f2fd65bc518fdb600db1'
		request["cache-control"] = 'no-cache'
		response = http.request(request)
		return response.read_body
	end


	def routing()

		param = {
			:"points[]1" => "-66.9126,10.5076",
			:"points[]2" => "-66.9162,10.5068",
			}


		url = URI("https://api.intraffic.com.ve/routing.geojson")
		url.query = URI.encode_www_form(param)
		http = Net::HTTP.new(url.host, url.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE

		request = Net::HTTP::Get.new(url)
		request["user-agent"] = 'develop'
		request["authorization"] = 'Bearer 951738bcef2c711bdbe7c2100f1d61eb63094620'
		request["cache-control"] = 'no-cache'
		request["compact"] = true
		response = http.request(request)
		return response.read_body
	end

	def pedro()

		


		url = URI("https://api.intraffic.com.ve/vehicles/get_vehicle_position.json/?vehicle_id=188403,189985")
		# url.query = URI.encode_www_form(param)
		http = Net::HTTP.new(url.host, url.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE

		request = Net::HTTP::Get.new(url)
		request["user-agent"] = 'develop'
		request["authorization"] = 'Bearer 5fa17393068cc4a5f5f1d50d7525f1c9e4333356'
		request["cache-control"] = 'no-cache'
		request["compact"] = true
		response = http.request(request)
		return response.read_body

	end


end
